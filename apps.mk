PRODUCT_PACKAGES += \
	org.schabi.newpipe \
	org.smssecure.smssecure \
	net.osmand.plus \
	org.mozilla.fennec_fdroid \
	ws.xsoh.etar \
	de.grobox.liberario \
	com.artifex.mupdf.viewer.app \
	com.aurora.store \
	com.fsck.k9 \
	com.etesync.syncadapter \
	com.nextcloud.client \
	org.tasks \
	org.mariotaku.twidere \
	com.pitchedapps.frost \
	com.keylesspalace.tusky \
	co.pxhouse.sas \
	com.simplemobiletools.gallery.pro \
	com.aurora.adroid \

